#ifndef WYBUCHOWA_H
#define WYBUCHOWA_H

#include <Kulka.h>

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>


class Wybuchowa : public Kulka
{
    public:
        Wybuchowa(int x, int y, int m, int n, int z, sf::Texture& tekstura_kulki);
        ~Wybuchowa() = default;

        Wybuchowa(const Wybuchowa& z) = default;
        Wybuchowa& operator= (const Wybuchowa& u) = default;

};

#endif // WYBUCHOWA_H
