#ifndef PRZYSPIESZAJACY_H
#define PRZYSPIESZAJACY_H

#include <Odbijacz.h>


class Przyspieszajacy : public Odbijacz
{
    public:
        Przyspieszajacy(int x, int y, char s, sf::Texture& tekstura_przyspieszajacy);

        ~Przyspieszajacy() = default;

        Przyspieszajacy(const Przyspieszajacy& u) = default;
        Przyspieszajacy& operator= (const Przyspieszajacy& u) = default;

        void kolizja(Kulka& kulka_2);

};

#endif // PRZYSPIESZAJACY_H
