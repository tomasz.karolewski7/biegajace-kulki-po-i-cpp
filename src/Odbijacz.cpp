#include <Kulka.h>

#include "Odbijacz.h"
#include "Kulka.h"

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

Odbijacz::Odbijacz(int x, int y, char s, sf::Texture& tekstura_odbijacza)
{
    wspx = x;
    wspy = y;
    strona = s;

    kierx = 0;
    kiery = 0;

    speed = 0;
    zyje = 1;
    animacja=1;


    sprite_odbijacza.setTexture(tekstura_odbijacza);

    sprite_odbijacza.setTextureRect(sf::IntRect(0, 0, 50, 50));
    sprite_odbijacza.setPosition(x*50, y*50);
}

bool Odbijacz::czy_kolizja(Kulka& kulka_2)
{

    if(kulka_2.zyje==0 || zyje==0)
    {
        return false;
    }
    else
    {
        if(wspx==kulka_2.wspx && wspy==kulka_2.wspy)
        {
            kulka_2.odbita_od_odbijacza=strona;
            return true;
        }

        else
        {
            kulka_2.odbita_od_odbijacza='0';
            return false;
        }

    }

}

void Odbijacz::smierc()
{
    zyje=0;
    speed=0;
}


