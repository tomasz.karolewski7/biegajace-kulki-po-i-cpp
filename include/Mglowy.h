#ifndef MGLOWY_H
#define MGLOWY_H

#include <Odbijacz.h>

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

class Mglowy : public Odbijacz
{
    public:
        Mglowy(int x, int y, char s, sf::Texture& tekstura_mglowy);

        ~Mglowy() = default;

        Mglowy(const Mglowy& u) = default;
        Mglowy& operator= (const Mglowy& u) = default;

        void kolizja(Kulka& kulka_2);

};

#endif // MGLOWY_H
