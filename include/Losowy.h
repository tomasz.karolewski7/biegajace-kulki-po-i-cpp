#ifndef LOSOWY_H
#define LOSOWY_H

#include <Odbijacz.h>


class Losowy : public Odbijacz
{
    public:
        Losowy(int x, int y, char s, sf::Texture& tekstura_losowy);

        ~Losowy() = default;

        Losowy(const Losowy& u) = default;
        Losowy& operator= (const Losowy& u) = default;

        void kolizja(Kulka& kulka_2);

};

#endif // LOSOWY_H
