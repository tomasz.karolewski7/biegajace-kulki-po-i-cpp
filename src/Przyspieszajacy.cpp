#include <iostream>
#include "Przyspieszajacy.h"

using namespace std;

Przyspieszajacy::Przyspieszajacy(int x, int y, char s, sf::Texture& tekstura_przyspieszajacy) : Odbijacz(x, y, s, tekstura_przyspieszajacy)
{

}

void Przyspieszajacy::kolizja(Kulka& kulka_2)
{
    if(czy_kolizja(kulka_2))
    {
        if(kulka_2.kierx!=0 && kulka_2.kiery!=0)
        {
            kulka_2.kierx *=-1;
            kulka_2.kiery *=-1;
        }
        else
        {
            if(strona=='l')
            {
                if(kulka_2.kiery!=0)
                {
                    kulka_2.kierx = -kulka_2.kiery;
                    kulka_2.kiery=0;
                }
                else
                {
                    kulka_2.kiery = -kulka_2.kierx;
                    kulka_2.kierx=0;
                }
            }
            else if(strona=='p')
             {
                if(kulka_2.kierx!=0)
                {
                    kulka_2.kiery = kulka_2.kierx;
                    kulka_2.kierx=0;
                }
                else
                {
                    kulka_2.kierx = kulka_2.kiery;
                    kulka_2.kiery=0;
                }
            }

        }
        kulka_2.speed*=2;
        if(kulka_2.speed > 1)
        {
            kulka_2.speed=1;
        }
        cout<<"odbijacz przyspieszajacy odbil"<<endl;
    }
}
