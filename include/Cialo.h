#ifndef CIALO_H
#define CIALO_H

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

class Cialo
{
    public:
        Cialo();


        Cialo(const Cialo& u) = default;
        Cialo& operator= (const Cialo& u) = default;

        ~Cialo() = default;

    protected:
        double wspx;
        double wspy;
        int kierx;
        int kiery;
        double speed;
        int zyje;
        int animacja;
        sf::Clock animacje;


};

#endif // CIALO_H
