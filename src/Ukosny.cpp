#include <iostream>

#include "Ukosny.h"
#include "Odbijacz.h"

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

using namespace std;

Ukosny::Ukosny(int x, int y, char s, sf::Texture& tekstura_ukosny) : Odbijacz(x, y, s, tekstura_ukosny)
{

}


void Ukosny::kolizja(Kulka& kulka_2)
{
    if(czy_kolizja(kulka_2))
    {
        if(kulka_2.kierx!=0 && kulka_2.kiery!=0)
        {
            kulka_2.kierx *=-1;
            kulka_2.kiery *=-1;
        }
        else
        {
            if(strona=='l')
            {
                if(kulka_2.kiery!=0)
                {
                    kulka_2.kierx = -kulka_2.kiery;
                    kulka_2.kiery=0;
                }
                else
                {
                    kulka_2.kiery = -kulka_2.kierx;
                    kulka_2.kierx=0;
                }
            }
            else if(strona=='p')
             {
                if(kulka_2.kierx!=0)
                {
                    kulka_2.kiery = kulka_2.kierx;
                    kulka_2.kierx=0;
                }
                else
                {
                    kulka_2.kierx = kulka_2.kiery;
                    kulka_2.kiery=0;
                }
            }

        }
        cout<<"odbijacz ukosny odbil"<<endl;
    }
}
