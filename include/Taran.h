#ifndef TARAN_H
#define TARAN_H

#include <Kulka.h>


class Taran : public Kulka
{
    public:
        Taran(int x, int y, int m, int n, int z, sf::Texture& tekstura_kulki);
        ~Taran() = default;

        Taran(const Taran& z) = default;
        Taran& operator= (const Taran& u) = default;

};

#endif // TARAN_H
