#include <iostream>
#include "Kulka.h"

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

using namespace std;

//////////////////// konstruktor ////////////////////

Kulka::Kulka(int x, int y, int m, int n, int z, sf::Texture& tekstura_kulki)
{
    wspx = x;
    wspy = y;
    zyje = z;
    animacja = 1;

    //////////w znaleznosci od polozenia nadajemy kulce odpowiedni kierunek lotu//////////

    if(x==m+1)
    {
        kierx = -1;
    }
    else if(x==0)
    {
        kierx = 1;
    }
    else
    {
        kierx = 0;
    }

    if(y==n+1)
    {
        kiery = -1;
    }
    else if(y==0)
    {
        kiery = 1;
    }
    else
    {
        kiery = 0;
    }


    speed = 1;

    sprite_kulki.setTexture(tekstura_kulki);

    sprite_kulki.setTextureRect(sf::IntRect(0, 0, 50, 50));
    sprite_kulki.setPosition(x*50, y*50);
}

bool Kulka::czy_kolizja(Kulka& kulka_2)
{

    if(zyje==0 || kulka_2.zyje==0)
    {
        return false;
    }
    else
    {
        if(wspx==kulka_2.wspx && wspy==kulka_2.wspy)
        {
            if(odbita_od_odbijacza=='l')
            {
                if((kierx==1 && kiery==-1) || (kulka_2.kierx==1 && kulka_2.kiery==-1) || (kierx==-1 && kiery==1) || (kulka_2.kierx==-1 && kulka_2.kiery==1))
                {
                    return false;
                }
                if(kierx==1 || kiery==1)
                {
                    if(kulka_2.kierx==-1 || kulka_2.kiery==-1)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                if(kierx==-1 || kiery==-1)
                {
                    if(kulka_2.kierx==1 || kulka_2.kiery==1)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                if(kulka_2.kierx==-1 || kulka_2.kiery==-1)
                {
                    if(kierx==1 || kiery==1)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                if(kulka_2.kierx==1 || kulka_2.kiery==1)
                {
                    if(kierx==-1 || kiery==-1)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
            }
            else if(odbita_od_odbijacza=='p')
            {
                if((kierx==1 && kiery==1) || (kulka_2.kierx==1 && kulka_2.kiery==1) || (kierx==-1 && kiery==-1) || (kulka_2.kierx==-1 && kulka_2.kiery==-1))
                {
                    return false;
                }
                if(kierx==1 || kiery==-1)
                {
                    if(kulka_2.kierx==-1 || kulka_2.kiery==1)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                if(kierx==-1 || kiery==1)
                {
                    if(kulka_2.kierx==1 || kulka_2.kiery==-1)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                if(kulka_2.kierx==-1 || kulka_2.kiery==1)
                {
                    if(kierx==1 || kiery==-1)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                if(kulka_2.kierx==1 || kulka_2.kiery==-1)
                {
                    if(kierx==-1 || kiery==1)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
            }
            else
            {
                return true;
            }
        }
        else
        {
            return false;
        }
    }

}

void Kulka::kolizja(Kulka& kulka_2)
{
    if(kierx==kulka_2.kierx && kiery==kulka_2.kiery)
    {
        if(speed > kulka_2.speed)
        {
            kierx*=-1;
            kiery*=-1;
        }
        else if(speed < kulka_2.speed)
        {
            kulka_2.kierx*=-1;
            kulka_2.kiery*=-1;
        }
        else if(speed == kulka_2.speed)
        {
            //prawo kto pierwszy ten lepszy
            /*kierx*=-1;
            kiery*=-1;*/
        }
        cout<<"przypadek 1"<<endl;
    }
    else if(kierx == kulka_2.kierx * (-1) && kiery== kulka_2.kiery* (-1))
    {
        kierx*=-1;
        kiery*=-1;

        kulka_2.kierx*=-1;
        kulka_2.kiery*=-1;
        cout<<"przypadek 2"<<endl;
    }
    else if(kierx * kulka_2.kierx ==0 && kiery * kulka_2.kiery ==0)
    {

        int a,b,c,d;
        a= kierx;
        b=kulka_2.kierx;

        c= kiery;
        d= kulka_2.kiery;

        kierx=b;
        kulka_2.kierx=a;

        kiery=d;
        kulka_2.kiery=c;

        cout<<"przypadek 3"<<endl;

    }
    else if((kierx * kulka_2.kierx ==0 && kiery * kulka_2.kiery ==-1) ||
            (kierx * kulka_2.kierx ==-1 && kiery * kulka_2.kiery ==0) ||
            (kierx * kulka_2.kierx ==1 && kiery * kulka_2.kiery ==-1) ||
            (kierx * kulka_2.kierx ==-1 && kiery * kulka_2.kiery ==1))
    {
        if(kierx*kulka_2.kierx==-1)
        {
            kierx*=-1;
            kulka_2.kierx*=-1;
        }
        else if(kiery*kulka_2.kiery==-1)
        {
            kiery*=-1;
            kulka_2.kiery*=-1;
        }
        cout<<"przypadek 4"<<endl;
    }
    else if((kierx * kulka_2.kierx ==0 && kiery * kulka_2.kiery ==1) ||
            (kierx * kulka_2.kierx ==1 && kiery * kulka_2.kiery ==0))
    {
        if(odbita_od_odbijacza=='0')
        {
            if(kierx*kulka_2.kierx==0)
            {
                kierx=0;
                kulka_2.kierx=0;

                kulka_2.kiery*=-1;
            }
            else if(kiery*kulka_2.kiery==0)
            {
                kiery=0;
                kulka_2.kiery=0;

                kulka_2.kierx*=-1;
            }
        }
        else
        {
            if(kierx == kiery || kierx == -kiery)
            {
                int tmp = kulka_2.kierx;
                kulka_2.kierx = kulka_2.kiery;
                kulka_2.kiery = tmp;
            }
            else if(kulka_2.kierx == kulka_2.kiery || kulka_2.kierx == -kulka_2.kiery)
            {
                int tmp2 = kierx;
                kierx = kiery;
                kiery = tmp2;
            }
        }

        cout<<"przypadek 5"<<endl;
    }
    double tmp=speed;
    speed=kulka_2.speed;
    kulka_2.speed=tmp;
}

bool Kulka::poza_mapa(int m, int n)
{
    if((wspx== 0 || wspy== 0 ||
       wspx== (m+1) || wspy== (n+1)) && (zaczela_ruch==true))
    {
        return true;
    }
    else
    {
        return false;
    }

}

void Kulka::smierc()
{
    zyje=0;
    speed=0;
}

void Kulka::drukuj()
{
    cout<<"wspolrzedna x: "<<wspx<<endl;
    cout<<"wspolrzedna y: "<<wspy<<endl;
    cout<<endl;
}

