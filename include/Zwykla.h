#ifndef ZWYKLA_H
#define ZWYKLA_H

#include <Kulka.h>

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

class Zwykla : public Kulka
{
    public:
        Zwykla(int x, int y, int m, int n, int z, sf::Texture& tekstura_kulki);
        ~Zwykla() = default;

        Zwykla(const Zwykla& z) = default;
        Zwykla& operator= (const Zwykla& u) = default;

};

#endif // ZWYKLA_H
