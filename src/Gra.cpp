#include "Gra.h"
#include "Ukosny.h"
#include "Zwykla.h"

#include <typeinfo>
#include <vector>
#include <iostream>
#include <fstream>
#include <ctype.h>
#include <stdexcept>
#include <windows.h>

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

using namespace std;

//konstruktor gry, ktory wczytuje plik i podczas generowania mapy sprawdza czy plansza jest prawidlowa
Gra::Gra(string nazwa)
{
    fstream plik;

    plik.open(nazwa, ios::in);
    if(plik.is_open()==true)
    {
        cout<<"udalo sie wczytac plik"<<endl;
        cout<<"rozpoczynam test pliku..."<<endl;


        string linia="";

        getline(plik, linia);

        int licznik_spacji = 0;

        for(int i=0; i<linia.size(); i++)
        {
            if(linia[i]==' ')
                licznik_spacji++;

            if(!isdigit(linia[i]) && linia[i]!=' ')
            {
                plik.close();
                cout<<linia<<endl;
                throw invalid_argument("linia przeznaczona na liczby zawiera inne znaki!");
            }
        }
        if(licznik_spacji!=2)
        {
            plik.close();
            cout<<linia<<endl;
            throw invalid_argument("linia przeznaczona na czas i wymiary nie zawiera 3 liczb!");
        }

        string liczby;
        int liczby_po_konwersji[3];

        unsigned int i=0;
        unsigned int j=0;
        while(i<3)
        {

            while(j < linia.size())
            {
                if(linia[j]==' ')
                {
                    j++;
                    break;
                }

                liczby+= linia[j];
                j++;
            }

            liczby_po_konwersji[i]= stoi(liczby);

            liczby = "";
            i++;
        }

        czas_do_wybuchu = liczby_po_konwersji[0];
        wymiarx = liczby_po_konwersji[1];
        wymiary = liczby_po_konwersji[2];
        if(czas_do_wybuchu<=0)
        {
            plik.close();
            throw invalid_argument("w pliku podana bledna wartosc czasu do wybuchu(<=0)");
        }
        if(wymiarx<=0)
        {
            plik.close();
            throw invalid_argument("w pliku podana bledna wartosc szerokosci planszy(<=0)");
        }
        if(wymiary<=0)
        {
            plik.close();
            throw invalid_argument("w pliku podana bledna wartosc wysokosci planszy(<=0)");
        }

        int tmp=0;
        while(!plik.eof())
        {
            getline(plik, linia);
            plansza.push_back(vector<char>());

            if(linia.size()!=wymiarx+2)
            {
                plik.close();
                cout<<"linia nr: "<<tmp+1<<", o tresci: '"<<linia<<"' ma dlugosc: "<<linia.size()<<" a powinna: "<<wymiarx+2<<endl;
                throw invalid_argument("w pliku jest linia innej dlugosci niz zadeklarowano!");
            }

            for(int j=0; j<wymiarx+2; j++)
            {
                if(linia[j]!='.' && linia[j]!='Z' && linia[j]!='T' && linia[j]!='W' && linia[j]!='U' &&
                   linia[j]!='u' && linia[j]!='L' && linia[j]!='l' && linia[j]!='z' && linia[j]!='S' &&
                   linia[j]!='s' && linia[j]!='M' && linia[j]!='m' && linia[j]!='P' && linia[j]!='p')
                {
                    plik.close();
                    cout<<"linia nr: "<<tmp+1<<", o tresci: '"<<linia<<"' zawiera niepoprawny znak: '"<<linia[j]<<"' upewnij sie, ze znaki w pliku nie sa inne niz (. T W U u L l Z z S s M m P p)"<<endl;
                    throw invalid_argument("w pliku znajduja sie nie obslugiwane znaki!");
                }
                if(tmp==0 || tmp==wymiary+1 || j==0 || j==wymiarx+1)
                {
                    if(linia[j]!='.' && linia[j]!='Z' && linia[j]!='T' && linia[j]!='W')
                    {
                        plik.close();
                        cout<<"linia nr: "<<tmp+1<<", o tresci: '"<<linia<<"' zawiera na krawedzi odbijacz: '"<<linia[j]<<"' upewnij sie, ze odbijacze znajduja sie na mapie nie krawedzi (dozwolone znaki: . T W Z)"<<endl;
                        throw invalid_argument("na krawedzi znajduja sie nie obslugiwane znaki!");
                    }
                }
                else
                {
                    if(linia[j]=='T' || linia[j]=='W')
                    {
                        plik.close();
                        cout<<"linia nr: "<<tmp+1<<", o tresci: '"<<linia<<"' zawiera kulke na planszy: '"<<linia[j]<<"' upewnij sie, ze kulki znajduja sie na krawedzi (dozwolone znaki: . U u L l Z z S s M m P p)"<<endl;
                        throw invalid_argument("na planszy znajduja sie nie obslugiwane znaki!");
                    }
                }
                plansza[tmp].push_back(linia[j]);
            }
            tmp++;
        }
        if(tmp!=wymiary+2)
        {
            plik.close();
            cout<<"w pliku znaleziono "<<tmp<<" linii"<<" a powinno byc: "<<wymiary+2<<endl;
            throw invalid_argument("w pliku jest inna liczba linii niz zadeklarowano, upewnij sie ze nie masz na koncu pliku pustych linii!");
        }

        plik.close();
        cout<<"plik wczytano poprawnie"<<endl;
    }
    else
    {
        throw invalid_argument("nie udalo sie otworzyc pliku");
    }



    oknoAplikacji.create( sf::VideoMode((wymiarx+2)*50, (wymiary+2)*50, 32 ), "Biegajace kulki" );
    tekstura_tla.loadFromFile( "graphic/tlo.png" );
    tekstura_krawedzi.loadFromFile( "graphic/tlo_start.png" );


    image_ikonka.loadFromFile("graphic/icon.png");
    oknoAplikacji.setIcon(image_ikonka.getSize().x, image_ikonka.getSize().y, image_ikonka.getPixelsPtr() );


    tlo.resize(wymiary+2);
    for (int i = 0; i < wymiary+2; ++i)
        tlo[i].resize(wymiarx+2);

    for(int i=0; i<wymiary+2; i++)
    {
        for(int j=0; j<wymiarx+2; j++)
        {
            if(i==0 || i==wymiary+1 || j==0 || j==wymiarx+1)
            {
                tlo[i][j].setTexture( tekstura_krawedzi );
                tlo[i][j].setPosition(j*50 , i*50 );
            }
            else
            {
                tlo[i][j].setTexture( tekstura_tla );
                tlo[i][j].setPosition(j*50 , i*50 );
            }

        }
    }

    ///grafika kulek///
    image_zwykla.loadFromFile("graphic/zwykla.png");
    image_zwykla.createMaskFromColor(sf::Color( 255, 255, 255));

    tekstura_zwykla.loadFromImage(image_zwykla);

    image_taran.loadFromFile("graphic/taran.png");
    image_taran.createMaskFromColor(sf::Color( 255, 255, 255));

    tekstura_taran.loadFromImage(image_taran);

    image_wybuchowa.loadFromFile("graphic/wybuchowa.png");
    image_wybuchowa.createMaskFromColor(sf::Color( 255, 255, 255));

    tekstura_wybuchowa.loadFromImage(image_wybuchowa);


    ///grafika odbijaczy///
    image_ukosny_l.loadFromFile("graphic/ukosny_l.png");
    image_ukosny_l.createMaskFromColor(sf::Color( 255, 255, 255));

    tekstura_ukosny_l.loadFromImage(image_ukosny_l);




    image_ukosny_p.loadFromFile("graphic/ukosny_p.png");
    image_ukosny_p.createMaskFromColor(sf::Color( 255, 255, 255));

    tekstura_ukosny_p.loadFromImage(image_ukosny_p);


    image_losowy_l.loadFromFile("graphic/losowy_l.png");
    image_losowy_l.createMaskFromColor(sf::Color( 255, 255, 255));

    tekstura_losowy_l.loadFromImage(image_losowy_l);


    image_losowy_p.loadFromFile("graphic/losowy_p.png");
    image_losowy_p.createMaskFromColor(sf::Color( 255, 255, 255));

    tekstura_losowy_p.loadFromImage(image_losowy_p);

    image_zjadajacy_l.loadFromFile("graphic/zjadajacy_l.png");
    image_zjadajacy_l.createMaskFromColor(sf::Color( 255, 255, 255));

    tekstura_zjadajacy_l.loadFromImage(image_zjadajacy_l);


    image_zjadajacy_p.loadFromFile("graphic/zjadajacy_p.png");
    image_zjadajacy_p.createMaskFromColor(sf::Color( 255, 255, 255));

    tekstura_zjadajacy_p.loadFromImage(image_zjadajacy_p);


    image_spowalaniajacy_l.loadFromFile("graphic/spowalniajacy_l.png");
    image_spowalaniajacy_l.createMaskFromColor(sf::Color( 255, 255, 255));

    tekstura_spowalniajacy_l.loadFromImage(image_spowalaniajacy_l);


    image_spowalaniajacy_p.loadFromFile("graphic/spowalniajacy_p.png");
    image_spowalaniajacy_p.createMaskFromColor(sf::Color( 255, 255, 255));

    tekstura_spowalniajacy_p.loadFromImage(image_spowalaniajacy_p);


    image_mglowy_l.loadFromFile("graphic/mglowy_l.png");
    image_mglowy_l.createMaskFromColor(sf::Color( 255, 255, 255));

    tekstura_mglowy_l.loadFromImage(image_mglowy_l);


    image_mglowy_p.loadFromFile("graphic/mglowy_p.png");
    image_mglowy_p.createMaskFromColor(sf::Color( 255, 255, 255));

    tekstura_mglowy_p.loadFromImage(image_mglowy_p);


    image_przyspieszajacy_l.loadFromFile("graphic/przyspieszajacy_l.png");
    image_przyspieszajacy_l.createMaskFromColor(sf::Color( 255, 255, 255));

    tekstura_przyspieszajacy_l.loadFromImage(image_przyspieszajacy_l);


    image_przyspieszajacy_p.loadFromFile("graphic/przyspieszajacy_p.png");
    image_przyspieszajacy_p.createMaskFromColor(sf::Color( 255, 255, 255));

    tekstura_przyspieszajacy_p.loadFromImage(image_przyspieszajacy_p);
    cout<<endl<<endl;
}

template<class A>
void Gra::wypisz_druk(vector<A>& pierwszy)
{
    int licznik=1;
    for(auto& kulka: pierwszy)
    {
        cout<<"Kulka "<<typeid(kulka).name()<<", numer "<<licznik<<endl;
        kulka.drukuj();
        licznik++;
    }
}

//wypelniamy tablice obiektow obiektami z pliku
void Gra::wypelnij_obiektami()
{
    int tmpi=0, tmpj=0;
    for(auto& wiersz: plansza)
    {
        for(auto& kolumna :wiersz)
        {
            if(kolumna=='Z')
            {
                if(tmpi==0 || tmpi==wymiary+1 || tmpj==0 || tmpj==wymiarx+1)
                {
                    zwykle.push_back(Zwykla(tmpj, tmpi, wymiarx, wymiary, 1, tekstura_zwykla));
                }
                else
                {
                    zjadajace.push_back(Zjadajacy(tmpj, tmpi, 'l' ,tekstura_zjadajacy_l));
                }
            }

            if(kolumna=='T')
            {
                tarany.push_back(Taran(tmpj, tmpi, wymiarx, wymiary, 2, tekstura_taran));
            }
            if(kolumna=='W')
            {
                wybuchowe.push_back(Wybuchowa(tmpj, tmpi, wymiarx, wymiary, 3, tekstura_wybuchowa));
            }


            if(kolumna=='U')
            {
                ukosne.push_back(Ukosny(tmpj, tmpi, 'l' ,tekstura_ukosny_l));
            }
            if(kolumna=='u')
            {
                ukosne.push_back(Ukosny(tmpj, tmpi, 'p' ,tekstura_ukosny_p));
            }
            if(kolumna=='M')
            {
                mglowe.push_back(Mglowy(tmpj, tmpi, 'l' ,tekstura_mglowy_l));
            }
            if(kolumna=='m')
            {
                mglowe.push_back(Mglowy(tmpj, tmpi, 'p' ,tekstura_mglowy_p));
            }
            if(kolumna=='L')
            {
                losowe.push_back(Losowy(tmpj, tmpi, 'l' ,tekstura_losowy_l));
            }
            if(kolumna=='l')
            {
                losowe.push_back(Losowy(tmpj, tmpi, 'p' ,tekstura_losowy_p));
            }
            if(kolumna=='z')
            {
                zjadajace.push_back(Zjadajacy(tmpj, tmpi, 'p' ,tekstura_zjadajacy_p));
            }
            if(kolumna=='S')
            {
                spowalniajace.push_back(Spowalniajacy(tmpj, tmpi, 'l' ,tekstura_spowalniajacy_l));
            }
            if(kolumna=='s')
            {
                spowalniajace.push_back(Spowalniajacy(tmpj, tmpi, 'p' ,tekstura_spowalniajacy_p));
            }
            if(kolumna=='p')
            {
                przyspieszajace.push_back(Przyspieszajacy(tmpj, tmpi, 'l' ,tekstura_przyspieszajacy_l));
            }
            if(kolumna=='P')
            {
                przyspieszajace.push_back(Przyspieszajacy(tmpj, tmpi, 'p' ,tekstura_przyspieszajacy_p));
            }


             tmpj++;
        }
        tmpi++;
        tmpj=0;
    }
    cout<<"Kulki zaczely w miejscu: "<<endl;
    wypisz_druk(zwykle);
    wypisz_druk(wybuchowe);
    wypisz_druk(tarany);

    czas_do_eksplozji.restart();
}

//po nastapieniu kolizji kulek rozdzielamy odpowiednio skutki kolizji kulek
template<class kula1, class kula2>
void Gra::akcja_kulki(kula1 &k1, kula2 &k2)
{
    if(k1.zyje!=0 && k2.zyje!=0)
    {
        if(k1.zyje==2 && k2.zyje==2)
        {
            k1.kolizja(k2);
        }
        else
        {
            if(k1.zyje==2)
            {
                k2.smierc();
                k2.animacja=2;
            }
            if(k2.zyje==2)
            {
                k1.smierc();
                k1.animacja=2;
            }
            if(k1.zyje!=2 && k2.zyje!=2)
            {
                k1.kolizja(k2);
            }
        }
    }

}

//po nastapieniu kolizji kulek rozdzielamy odpowiednio skutki kolizji kulek z odbijaczem
template<class odbija, class kula>
void Gra::akcja_odbijacza(odbija &o, kula &k)
{
    if(k.zyje!=0)
    {
        if(k.zyje==2)
        {
            o.smierc();
            o.animacja=0;
        }
        else if(k.zyje==3)
        {
            if(czas_do_wybuchu <= czas_do_eksplozji.getElapsedTime().asSeconds())
            {
                o.smierc();
                o.animacja=0;

                k.smierc();
                k.animacja=2;
            }
            else
            {
                o.kolizja(k);
            }
        }
        else
        {
            o.kolizja(k);
        }
    }

}
//przelec po tablicy kulek i porownaj je ze soba
template<class A, class B>
void Gra::porownaj_kk(vector<A>& pierwszy, vector<B>& drugi)
{
    for(auto& i: pierwszy)
    {
        for(auto& j: drugi)
        {
            if(i.czy_kolizja(j))
            {
                akcja_kulki(i, j);
            }
        }
    }
}
//przelec po odbijaczach i kulkach w celu znalezienia kolizji
template<class C, class D>
void Gra::porownaj_ok(vector<C>& pierwszy, vector<D>& drugi)
{
    for(auto& i: pierwszy)
    {
        for(auto& j: drugi)
        {
            if(i.czy_kolizja(j))
            {
                akcja_odbijacza(i, j);
            }
        }
    }
}
//porownaj kulki tego samego rodzaju
template<class Z>
void Gra::porownaj_k(vector<Z>& pierwszy)
{
    for(int i=0; i<pierwszy.size(); i++)
    {
        for(int j=i+1; j<pierwszy.size(); j++)
        {
            if(pierwszy[i].czy_kolizja(pierwszy[j]))
            {
                akcja_kulki(pierwszy[i], pierwszy[j]);
            }
        }
    }
}

bool Gra::czy_okno_otwarte()
{
    return oknoAplikacji.isOpen();
}

void Gra::wylacz_program()
{
    while(oknoAplikacji.pollEvent(zdarzenie))
    {
        if(zdarzenie.type == sf::Event::Closed)
        {
            oknoAplikacji.close();
        }
    }
}

void Gra::display()
{
    oknoAplikacji.display();

    for(auto& v: tlo)
    {
        for(auto& i :v)
        {
            oknoAplikacji.draw(i);
        }
    }
}
// logika kulek i ich animacji
template<class K, typename F>
void Gra::przejzyj(vector<K> &pierwsza, F &flaga)
{
    for(auto& k: pierwsza)
    {
        if(k.poza_mapa(wymiarx, wymiary))
        {
            k.smierc();
            k.animacja=0;
        }
        if(k.animacja != 0)
        {
            if(k.animacja==2)
            {
                k.sprite_kulki.setTextureRect(sf::IntRect(50, 0, 50, 50));

                if(k.animacje.getElapsedTime().asSeconds() > 0.5f)
                {
                    k.animacja=3;
                    k.animacje.restart();
                }
            }
            else if(k.animacja==3)
            {
                k.sprite_kulki.setTextureRect(sf::IntRect(100, 0, 50, 50));

                if(k.animacje.getElapsedTime().asSeconds() > 0.5f)
                {
                    k.animacja=0;
                    k.animacje.restart();
                }
            }
            else if(k.animacja==1)
            {
                k.animacje.restart();
            }
            oknoAplikacji.draw(k.sprite_kulki);
            flaga=1;
        }
    }
}

void Gra::czy_sa_kulki()
{
    int flaga=0;

    przejzyj(zwykle, flaga);
    przejzyj(tarany, flaga);
    przejzyj(wybuchowe, flaga);

    if(flaga==0)
    {
        if(opoznij_zamkniecie.getElapsedTime().asSeconds() > 1.5f)
        {
            cout<<endl;
            cout<<"Kulki skonczyly w miejscu: "<<endl;
            wypisz_druk(zwykle);
            wypisz_druk(wybuchowe);
            wypisz_druk(tarany);

            oknoAplikacji.close();
        }
    }
    else
    {
        opoznij_zamkniecie.restart();
    }



}

template<class R>
void Gra::ruch_kulek(vector<R>& r)
{
    for(auto& i: r)
    {

        if(czas_do_wybuchu <= czas_do_eksplozji.getElapsedTime().asSeconds() && i.zyje==3 && i.animacja==1)
        {
            i.smierc();
            i.animacja=2;
        }

        i.sprite_kulki.move(i.speed * i.kierx*25, i.speed * i.kiery*25);
        i.wspx+= i.speed* i.kierx/2;
        i.wspy+= i.speed* i.kiery/2;
        i.zaczela_ruch = true;
    }
}


void Gra::wykonaj_kolizje()
{

    porownaj_ok(ukosne, zwykle);
    porownaj_ok(ukosne, tarany);
    porownaj_ok(ukosne, wybuchowe);

    porownaj_ok(mglowe, zwykle);
    porownaj_ok(mglowe, tarany);
    porownaj_ok(mglowe, wybuchowe);

    porownaj_ok(losowe, zwykle);
    porownaj_ok(losowe, tarany);
    porownaj_ok(losowe, wybuchowe);

    porownaj_ok(zjadajace, zwykle);
    porownaj_ok(zjadajace, tarany);
    porownaj_ok(zjadajace, wybuchowe);

    porownaj_ok(przyspieszajace, zwykle);
    porownaj_ok(przyspieszajace, tarany);
    porownaj_ok(przyspieszajace, wybuchowe);

    porownaj_ok(spowalniajace, zwykle);
    porownaj_ok(spowalniajace, tarany);
    porownaj_ok(spowalniajace, wybuchowe);

    porownaj_k(zwykle);
    porownaj_k(tarany);
    porownaj_k(wybuchowe);

    porownaj_kk(zwykle, tarany);
    porownaj_kk(zwykle, wybuchowe);
    porownaj_kk(tarany, wybuchowe);

    odbicie.restart();

    ruch_kulek(zwykle);
    ruch_kulek(tarany);
    ruch_kulek(wybuchowe);

}
//logika odbijaczy i ich animacji
template<class O>
void Gra::przejdz_po_odbijaczach(vector<O>& o)
{
    for(auto& i: o)
    {
        if(i.animacja!=0)
        {
            if(i.animacja==1)
            {
                i.sprite_odbijacza.setTextureRect(sf::IntRect(50, 0, 50, 50));

                if(i.animacje.getElapsedTime().asSeconds() > 0.5f)
                {
                    i.animacja=2;
                    i.animacje.restart();
                }
            }
            else if(i.animacja==2)
            {
                i.sprite_odbijacza.setTextureRect(sf::IntRect(100, 0, 50, 50));

                if(i.animacje.getElapsedTime().asSeconds() > 0.5f)
                {
                    i.animacja=3;
                    i.animacje.restart();
                }
            }
            else if(i.animacja==3)
            {
                i.sprite_odbijacza.setTextureRect(sf::IntRect(150, 0, 50, 50));

                if(i.animacje.getElapsedTime().asSeconds() > 0.5f)
                {
                    i.animacja=4;
                    i.animacje.restart();
                }
            }
            else if(i.animacja==4)
            {
                i.sprite_odbijacza.setTextureRect(sf::IntRect(0, 0, 50, 50));

                if(i.animacje.getElapsedTime().asSeconds() > 0.5f)
                {
                    i.animacja=1;
                    i.animacje.restart();
                }
            }
            oknoAplikacji.draw(i.sprite_odbijacza);
        }
    }
}

void Gra::rysowanie_odbijaczy()
{
    przejdz_po_odbijaczach(ukosne);
    przejdz_po_odbijaczach(mglowe);
    przejdz_po_odbijaczach(losowe);
    przejdz_po_odbijaczach(zjadajace);
    przejdz_po_odbijaczach(przyspieszajace);
    przejdz_po_odbijaczach(spowalniajace);
}
