#include <iostream>
#include "Losowy.h"

using namespace std;

Losowy::Losowy(int x, int y, char s, sf::Texture& tekstura_losowy) : Odbijacz(x, y, s, tekstura_losowy)
{

}

void Losowy::kolizja(Kulka& kulka_2)
{
    int losowy_kierunek = rand() % 3;

    if(czy_kolizja(kulka_2))
    {
        if(strona=='l')
        {
            if((kulka_2.kierx==-1 && kulka_2.kiery==1) || (kulka_2.kierx==1 && kulka_2.kiery==-1))
            {
                kulka_2.kierx *=-1;
                kulka_2.kiery *=-1;
            }
            else
            {
                if(kulka_2.kierx==-1 || kulka_2.kiery==-1)
                {
                    if(losowy_kierunek==0)
                    {
                        kulka_2.kierx = 0;
                        kulka_2.kiery = 1;
                    }
                    else if(losowy_kierunek==1)
                    {
                        kulka_2.kierx = 1;
                        kulka_2.kiery = 0;
                    }
                    else
                    {
                        kulka_2.kierx = 1;
                        kulka_2.kiery = 1;
                    }
                }
                else
                {
                    if(losowy_kierunek==0)
                    {
                        kulka_2.kierx = 0;
                        kulka_2.kiery = -1;
                    }
                    else if(losowy_kierunek==1)
                    {
                        kulka_2.kierx = -1;
                        kulka_2.kiery = 0;
                    }
                    else
                    {
                        kulka_2.kierx = -1;
                        kulka_2.kiery = -1;
                    }
                }
            }
        }
        else if(strona=='p')
         {
            if((kulka_2.kierx==-1 && kulka_2.kiery==-1) || (kulka_2.kierx==1 && kulka_2.kiery==1))
            {
                kulka_2.kierx *=-1;
                kulka_2.kiery *=-1;
            }
            else
            {
                if(kulka_2.kierx==-1 || kulka_2.kiery==1)
                {
                    if(losowy_kierunek==0)
                    {
                        kulka_2.kierx = 0;
                        kulka_2.kiery = -1;
                    }
                    else if(losowy_kierunek==1)
                    {
                        kulka_2.kierx = 1;
                        kulka_2.kiery = 0;
                    }
                    else
                    {
                        kulka_2.kierx = 1;
                        kulka_2.kiery = -1;
                    }
                }
                else
                {
                    if(losowy_kierunek==0)
                    {
                        kulka_2.kierx = 0;
                        kulka_2.kiery = 1;
                    }
                    else if(losowy_kierunek==1)
                    {
                        kulka_2.kierx = -1;
                        kulka_2.kiery = 0;
                    }
                    else
                    {
                        kulka_2.kierx = -1;
                        kulka_2.kiery = 1;
                    }
                }
            }
        }

    cout<<"odbijacz losowy odbil"<<endl;
    }
}
