#ifndef KULKA_H
#define KULKA_H

#include <Cialo.h>

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>


class Kulka : public Cialo
{
    public:

        Kulka(int x, int y, int m, int n, int z, sf::Texture& tekstura_kulki);

        Kulka(const Kulka& k) = default;
        Kulka& operator= (const Kulka& k) = default;

        ~Kulka() = default;

        bool czy_kolizja(Kulka& kulka_2);

        void kolizja(Kulka& kulka_2);


        bool poza_mapa(int m, int n);

        void smierc();

        char odbita_od_odbijacza = '0';


        void drukuj();

        bool zaczela_ruch = false;


        sf::Sprite sprite_kulki;
       // sf::Clock clock_kulki;



};

#endif // KULKA_H
