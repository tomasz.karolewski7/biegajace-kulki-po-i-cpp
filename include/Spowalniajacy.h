#ifndef SPOWALNIAJACY_H
#define SPOWALNIAJACY_H

#include <Odbijacz.h>


class Spowalniajacy : public Odbijacz
{
    public:
        Spowalniajacy(int x, int y, char s, sf::Texture& tekstura_spowalniajacy);

        ~Spowalniajacy() = default;

        Spowalniajacy(const Spowalniajacy& u) = default;
        Spowalniajacy& operator= (const Spowalniajacy& u) = default;

        void kolizja(Kulka& kulka_2);

};

#endif // SPOWALNIAJACY_H
