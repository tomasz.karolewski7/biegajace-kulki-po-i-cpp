#ifndef ODBIJACZ_H
#define ODBIJACZ_H

#include <Cialo.h>
#include <Kulka.h>

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

class Odbijacz : public Cialo
{
    public:
        Odbijacz(int x, int y, char s, sf::Texture& tekstura_odbijacza);

        Odbijacz(const Odbijacz& u) = default;
        Odbijacz& operator= (const Odbijacz& u) = default;

        ~Odbijacz() = default;

        sf::Sprite sprite_odbijacza;

        bool czy_kolizja(Kulka& kulka_2);

        virtual void kolizja(Kulka& kulka_2)=0;

        void smierc();

        char strona;

};

#endif // ODBIJACZ_H
