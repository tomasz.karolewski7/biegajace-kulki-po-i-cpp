///Tomasz Karolewski 417668
///program korzysta z biblioteki SFML: https://www.sfml-dev.org/download.php
///Program zawiera nastÍpujace zalozenia:
///-kulki odbijaja sie parami
///-odbijacz oddzialowywuje swoja moca na kulke nawet jak ta odbije sie od jej krotszej krawedzi(np zjadajacy, spowalniajacy) poza odbijaczem losowym
///-odbijacze z mocami zmieniaja zwrot kulki po odbiciu tak samo jak ukosny tylko maja wiecej skuktow ubocznych
///-w pliku jako pusta przestrzen sa uzwane kropki nie spacja, by ulatwic wpisywanie danych(przyklad w pliku test.txt)
///-jedna tura to jedna sekunda(czyli kulka leci z predkoscia jedno pole na sekunde)
///-kulka podczas odbicia ma takie same wspolrzedne jak obiekt z ktorym sie odbija
///
///kazdy z stworzonych obiektow nie jest kopiowany pomimo tego mamy konstruktor kopiujacy, operator przypisania
///obiekt klas ktore napisalem sie "dobrze niszcza" to znaczy ze klasyczny destruktor im wystarcza
///vectory same dobrze sie psuja
///klasy nie maja getterow i seterow a wiec ich pola sa publiczne. Wybralem te droge poniewaz uwazam ze dane sa bezpieczne bo uzytkownik i tak nie ma do nich dostepu poniewaz wszystkim steruje klasa Gra
///pierwsza zmienna w pliku to czas do wybuchu kulek wybuchowych jednak symulacja konczy sie dopiero gdy wszystkie kulki "umra"(znikna z planszy)

#include <iostream>
#include <fstream>
#include <time.h>

#include "Cialo.h"
#include "Odbijacz.h"
#include "Kulka.h"
#include "Gra.h"

#include <array>
#include <vector>
#include <fstream>

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

using namespace std;


int main()
{
    srand(time(NULL));

    string nazwa;

    int flaga=0;
    while(flaga==0)
    {
        cout<<"podaj nazwe pliku do wczytania: ";
        cin>>nazwa;

        fstream plik;

        plik.open(nazwa, ios::in);
        if(plik.is_open()==true)
        {
            plik.close();
            flaga=1;
        }
        else
        {
            cout<<"nie ma takiego pliku w folderze"<<endl;
        }
    }

    Gra gra(nazwa);
    gra.wypelnij_obiektami();


    ///////////glowna petla programu///////////
    while(gra.czy_okno_otwarte())
    {

        gra.wylacz_program();

        gra.display();


        gra.czy_sa_kulki();

        if(gra.odbicie.getElapsedTime().asSeconds() >= 0.5f)
        {
            gra.wykonaj_kolizje();
            gra.odbicie.restart();
        }

        gra.rysowanie_odbijaczy();
    }
    cout<<"koniec dzialania programu"<<endl;

    return 0;
}
