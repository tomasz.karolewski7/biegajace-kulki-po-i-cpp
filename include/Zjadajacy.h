#ifndef ZJADAJACY_H
#define ZJADAJACY_H

#include <Odbijacz.h>


class Zjadajacy : public Odbijacz
{
    public:
        Zjadajacy(int x, int y, char s, sf::Texture& tekstura_zjadajacy);

        ~Zjadajacy() = default;

        Zjadajacy(const Zjadajacy& u) = default;
        Zjadajacy& operator= (const Zjadajacy& u) = default;

        void kolizja(Kulka& kulka_2);

};

#endif // ZJADAJACY_H
