#ifndef UKOSNY_H
#define UKOSNY_H

#include <Odbijacz.h>

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>


class Ukosny : public Odbijacz
{
    public:
        Ukosny(int x, int y, char s, sf::Texture& tekstura_ukosny);

        ~Ukosny() = default;

        Ukosny(const Ukosny& u) = default;
        Ukosny& operator= (const Ukosny& u) = default;

        void kolizja(Kulka& kulka_2);

};

#endif // UKOSNY_H
