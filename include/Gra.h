#ifndef GRA_H
#define GRA_H

#include <iostream>
#include <vector>

#include "Ukosny.h"
#include "Mglowy.h"
#include "Losowy.h"
#include "Zjadajacy.h"
#include "Spowalniajacy.h"
#include "Przyspieszajacy.h"

#include "Zwykla.h"
#include "Wybuchowa.h"
#include "Taran.h"

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

using namespace std;

class Gra
{
    protected:
        int czas_do_wybuchu = 0;
        int wymiarx = 0;
        int wymiary = 0;

        vector<vector<char>> plansza;

        ///tablice kulek i odbijaczy///
        vector<Zwykla> zwykle;
        vector<Taran> tarany;
        vector<Wybuchowa> wybuchowe;

        vector<Ukosny> ukosne;
        vector<Mglowy> mglowe;
        vector<Losowy> losowe;
        vector<Zjadajacy> zjadajace;
        vector<Spowalniajacy> spowalniajace;
        vector<Przyspieszajacy> przyspieszajace;


        sf::Clock czas_do_eksplozji;

        sf::RenderWindow oknoAplikacji;

        sf::Image image_ikonka;

        vector<vector<sf::Sprite>> tlo;

        sf::Texture tekstura_tla;
        sf::Texture tekstura_krawedzi;

        ///tekstura kulek///
        sf::Image image_zwykla;
        sf::Texture tekstura_zwykla;

        sf::Image image_taran;
        sf::Texture tekstura_taran;

        sf::Image image_wybuchowa;
        sf::Texture tekstura_wybuchowa;

        ///tekstura odbijaczy///
        sf::Image image_ukosny_l;
        sf::Texture tekstura_ukosny_l;

        sf::Image image_ukosny_p;
        sf::Texture tekstura_ukosny_p;

        sf::Image image_losowy_l;
        sf::Texture tekstura_losowy_l;

        sf::Image image_losowy_p;
        sf::Texture tekstura_losowy_p;

        sf::Image image_zjadajacy_l;
        sf::Texture tekstura_zjadajacy_l;

        sf::Image image_zjadajacy_p;
        sf::Texture tekstura_zjadajacy_p;

        sf::Image image_spowalaniajacy_l;
        sf::Texture tekstura_spowalniajacy_l;

        sf::Image image_spowalaniajacy_p;
        sf::Texture tekstura_spowalniajacy_p;

        sf::Image image_mglowy_l;
        sf::Texture tekstura_mglowy_l;

        sf::Image image_mglowy_p;
        sf::Texture tekstura_mglowy_p;

        sf::Image image_przyspieszajacy_l;
        sf::Texture tekstura_przyspieszajacy_l;

        sf::Image image_przyspieszajacy_p;
        sf::Texture tekstura_przyspieszajacy_p;


        sf::Event zdarzenie;

        sf::Clock opoznij_zamkniecie;

    public:
        sf::Clock odbicie;
        Gra(string nazwa);

        Gra(const Gra& u) = default;
        Gra& operator= (const Gra& u) = default;

        ~Gra() = default;


        void wypelnij_obiektami();

        template<class A>
        void wypisz_druk(vector<A>& pierwszy);


        template<class A, class B>
        void porownaj_kk(vector<A>& pierwszy, vector<B>& drugi);

        template<class C, class D>
        void porownaj_ok(vector<C>& pierwszy, vector<D>& drugi);

        template<class Z>
        void porownaj_k(vector<Z>& pierwszy);

        template<class K, typename F>
        void przejzyj(vector<K> &pierwsza, F &flaga);

        void czy_sa_kulki();

        bool czy_okno_otwarte();

        void wylacz_program();

        void display();

        void wykonaj_kolizje();

        void rysowanie_odbijaczy();

        template<class O>
        void przejdz_po_odbijaczach(vector<O>& o);

        template<class R>
        void ruch_kulek(vector<R>& r);

        template<class kula1, class kula2>
        void akcja_kulki(kula1 &k1, kula2 &k2);

        template<class odbija, class kula>
        void akcja_odbijacza(odbija &o, kula &k);



};

#endif // GRA_H
